console.log("qs");

const text = document.querySelector(".text-bis");
const textContainer = text.closest(".texts");
const THRESHOLD = 70;

function handleHover(e) {
    const { clientX, clientY, currentTarget } = e;
    const { clientWidth, clientHeight, offsetLeft, offsetTop } = currentTarget;

    const touchLeft = clientX - textContainer.offsetLeft;
    const touchRight = clientWidth - clientX + textContainer.offsetLeft;
    const touchTop = clientY - textContainer.offsetTop;
    const touchBottom = clientHeight - clientY + textContainer.offsetTop;
    const touchs = [touchLeft, touchRight, touchTop, touchBottom].filter((el) => el < THRESHOLD);
    const size = touchs.length ? Math.min(...touchs) : THRESHOLD;
    const opacity = size / THRESHOLD;

    const horizontal = (clientX - textContainer.offsetLeft) / clientWidth;
    const vertical = (clientY - textContainer.offsetTop) / clientHeight;
    const horizontalPercent = Math.round(horizontal * 10000) / 100;
    const verticalPercent = Math.round(vertical * 10000) / 100;

    text.style.setProperty("--opacity", opacity);
    text.style.setProperty(
        "--circle",
        `circle(${size}px at ${horizontalPercent}% ${verticalPercent}%)`
    );
}

function resetStyles(e) {
    //text.style.opacity = 0;
}

text.addEventListener("mousemove", handleHover);
text.addEventListener("mouseleave", resetStyles);

